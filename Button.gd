extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var Selected : bool setget SetSelected
onready var MyLabel = $Label

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func SetSelected(selected : bool):
	if selected :
		MyLabel.add_color_override("font_color", Color(0, 0, 0))
		modulate = Color(1, 1, 1, 1)
	else:
		MyLabel.add_color_override("font_color", Color(0.25, 0.25, 0.25))
		modulate = Color(1, 1, 1, 0.5)

