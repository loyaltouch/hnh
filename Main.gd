extends Node2D

var StatusList : Array
var MemberList = {}
var SelIndex = 0
var animating = false

var PartyList = []
var RestList = []
var Selects = []

onready var SelButtonScene = load("res://Button.tscn")
onready var SelectPanel = $SelectPanel
onready var SelectPanelOrgPos = SelectPanel.position
onready var Title = $TitleWindow/Title
onready var Message = $MassageWindow/Message

# Called when the node enters the scene tree for the first time.
func _ready():
	Selects = [
		"選択肢1",
		"選択肢2",
		"選択肢3",
		"選択肢4",
		"選択肢5",
		"選択肢6",
	]
	setupSelButton()

func refreshSelButtons():
	var index = 0
	for child in SelectPanel.get_children():
		child.Selected = (index == SelIndex)
		index += 1

func setupSelButton():
	var selButtons = []
	for child in SelectPanel.get_children():
		child.visible = false
		selButtons.append(child)
	
	var index = 0
	var selButtonSize = selButtons.size()
	for selItem in Selects:
		var button
		if selButtonSize > index:
			button = selButtons[index]
		else:
			button = SelButtonScene.instance()
			SelectPanel.add_child(button)
			button.position = Vector2(400, 300 + index * 30)
			selButtons.append(button)
		button.visible = true
		button.get_node("Label").text = selItem
		index += 1
	SelectPanel.position = SelectPanelOrgPos
	SelIndex = 0
	refreshSelButtons()
